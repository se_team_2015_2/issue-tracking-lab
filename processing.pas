unit processing;

interface

const
    MatrixSize = 7;
    HighBound = 22;
    LowBound = -20;
    NumbersQuantity = HighBound - LowBound + 1;

type
    SquareMatrix = array [1..MatrixSize, 1..MatrixSize] of Integer;

function GetNumber: Integer;
procedure FillMatrix(var matrix: SquareMatrix);
function GetCountSecondaryDiagonalPositive(var matrix: SquareMatrix):Integer;
procedure SetToZeroUnderMainDiagonalNegative(var matrix: SquareMatrix);

implementation

function GetNumber: Integer;
begin
    GetNumber := Random(NumbersQuantity) + LowBound;
end;

procedure FillMatrix(var matrix: SquareMatrix);
var
    i, j: Integer;
begin
    for i := 1 to MatrixSize do
    begin
        for j := 1 to MatrixSize do
        begin
            matrix[i, j] := GetNumber;
        end;
    end;
end;

function GetCountSecondaryDiagonalPositive(var matrix: SquareMatrix):Integer;
var
    i, j, amount, counter: Integer;
begin
    amount := 0;
    counter := 0;
    for i := 1 to MatrixSize do
    begin
        for j := MatrixSize - counter downto Matrixsize - counter do
        begin
            if matrix[i, j] > 0 then
            begin
                amount := amount + 1;
            end;
        end;
        counter := counter + 1;
    end;
    GetCountSecondaryDiagonalPositive := amount;
end;

procedure SetToZeroUnderMainDiagonalNegative(var matrix: SquareMatrix);
var
    i, j, count: Integer;
begin
    count := 1;
    for i := count + 1 to MatrixSize do
    begin
        for j := 1 to count do
        begin
            if matrix[i, j] < 0 then
            begin
                matrix[i, j] := 0;
            end;
        end;
        count := count + 1;
    end;
end;

end.

