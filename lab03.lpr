program lab03;

uses processing, output;

var
    matrix: SquareMatrix;

begin
    FillMatrix(matrix);
    PrintMatrix(matrix);
    WriteLn;
    PrintNumber(GetCountSecondaryDiagonalPositive(matrix));
    SetToZeroUnderMainDiagonalNegative(matrix);
    WriteLn;
    PrintMatrix(matrix);
    ReadLn;
end.

